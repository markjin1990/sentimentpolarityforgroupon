import json
import nltk
import re
from nltk.corpus import stopwords
from nltk import stem
from sklearn.svm import SVC
import numpy as np
import csv


json_file = 'groupon_data.json'
output_file = 'groupon_vector2.json'
learn_ratio = 0.07
def readFile(file_name):
	input_file = open(file_name,'r');
	data_list = []
	for line in input_file:
		mydict = json.loads(line)
		mylist = []
		mylist.append(mydict['label'])
		mylist.append(mydict['list'])
		data_list.append(mylist)
	input_file.close()
	return data_list

def stem_and_remove_stopword(sent):
	ret_list = []
	sent = sent.lower()
	tokenized_list = nltk.word_tokenize(sent)
	stemmer = stem.snowball.EnglishStemmer()

	for word in tokenized_list:
		if re.match("[a-z]*$",word): 
			if(word[-1]=='.' or word[-1]==','):
				word = word[:-1]
			word = stemmer.stem(word)
			if word not in stopwords.words('english'):
				ret_list.append(word)
			
	return ret_list

def Get_Word_Bank(mylist,index):
	word_list = []
	for i in range(0,index):
		word_list.extend(mylist[i][1])
	return list(set(word_list))

def LabelList(comment_list,index):
	label_list = []
	for i in range(0,index):
		label_list.append(int(comment_list[i][0]));
	return label_list;

def IndexCommentList(comment_list,index_list,index):
	size = len(index_list)
	global output_file
	outfile = open(output_file,"wb")

	# Write the size of dimensions in the first line
	size_dict = dict()
	size_dict["dimension_size"] = len(index_list)
	json_dump = json.dumps(size_dict)
	outfile.write(json_dump+'\n') 
	

	count = 0
	for i in range(0,index):
		count = count + 1
		if count % 100 == 0:
			print count
		
		# Initialize each vector
		new_list = []
		vector_dict = dict()
		#for x in range(0,size):
			#new_list.append(0)
		
		#print comment_list[i][1]
		for word in comment_list[i][1]:
			
			index = index_list.index(word)
			if index in vector_dict:
				vector_dict[index] = vector_dict[index] + 1
			else:
				vector_dict[index] = 1		

		#for k in range(0,size):
		#	if new_list[k]>0:
		#		vector_dict[k] = new_list[k]
		json_dump = json.dumps(vector_dict)
		outfile.write(json_dump+'\n') 
	outfile.close()

data_list = readFile(json_file)
learn_index = int(len(data_list)*learn_ratio)
# Collect all words into sets "word_bank" in top learn_index comments
word_bank = Get_Word_Bank(data_list,learn_index) 
print len(word_bank)

# Build vectors for each top learn_index comments in the vector space consisting of the words in "word_bank"
IndexCommentList(data_list,word_bank,learn_index)





