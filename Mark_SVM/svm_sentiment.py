import json
import nltk
import re
from nltk.corpus import stopwords
from nltk import stem
from sklearn.svm import LinearSVC
import numpy as np
import csv


json_file = 'groupon_data.json'
json_vector_file = 'groupon_vector2.json'

num_of_learning_vector = 0

train_ratio = 0.55
test_ratio = 0.10
def readFile(file_name):
	input_file = open(file_name,'r');
	data_list = []
	for line in input_file:
		mydict = json.loads(line)
		mylist = []
		mylist.append(mydict['label'])
		mylist.append(mydict['list'])
		data_list.append(mylist)
	input_file.close()
	return data_list

def readVector(file_name):
	input_file = open(file_name,'r');
	vector_list = []
	dimension = 0
	count = 0
	for line in input_file:
		mydict = json.loads(line)
		if count == 0:
			dimension = int(mydict["dimension_size"])
		else:
			vector = []
			# initialize the vector to 0 in all dimensions
			for i in range(0,dimension):
				vector.append(0)
			for key,value in mydict.iteritems():
				vector[int(key)] = int(value)
	
			vector_list.append(vector)
		count = count + 1
	input_file.close()
	return vector_list

data_list = readFile(json_file)
vector_list = readVector(json_vector_file)
vector_size = len(vector_list)
label_list = []
for i in range(0,vector_size):
	label_list.append(int(data_list[i][0]))

# Start learning
train_index = int(train_ratio*len(vector_list))
test_index = int(test_ratio*len(vector_list))
#clf = SVC(kernel='linear',class_weight={1:0.8})
clf = LinearSVC(C=1.0, loss='l1',random_state=1)
print "Train on "+str(train_index)+" comments"

clf.fit(np.array(vector_list[:train_index]),np.array(label_list[:train_index]))
print "Training Finished"
print "Test on "+str(test_index)+" comments"
pred = clf.predict(np.array(vector_list[train_index+1:train_index+test_index]))

count = 0
tp = 0
fp = 0
fn = 0
tn = 0
print "Predict/Actual"
for item in label_list[train_index+1:train_index+test_index]:
	print str(pred[count])+"/"+str(item)
	predict = pred[count]
	label = item
	if(label==predict and predict == 1):
			tp = tp + 1
	if(label==predict and predict == 0):
			tn = tn + 1
	if(label!=predict and predict == 1):
			fp = fp + 1
	if(label!=predict and predict == 0):
			fn = fn + 1
	count = count + 1
precision = 1
if((tp+fp)!=0):
	precision = float(tp)/float(tp+fp)

recall = 1
if((tp+fn)!=0):
	recall = float(tp)/float(tp+fn)

accuracy = float(tp+tn)/float(tp+tn+fp+fn)
f1 = 2*precision*recall/(precision+recall)
print "True Positive: "+str(tp)
print "True Negative: "+str(tn)
print "False Positive: "+str(fp) 
print "False Negative: "+str(fn) 
print "Precision: "+str(precision)
print "Recall: "+str(recall)
print "Accuracy: "+str(accuracy)
print "F1: "+str(f1)
#learn_index = int(len(data_list)*learn_ratio)





