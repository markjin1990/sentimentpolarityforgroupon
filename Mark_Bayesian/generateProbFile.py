import json
import re
from random import choice
from collections import Counter

json_file = 'groupon_data.json'
learn_ratio = 0.7
def readFile(file_name):
	input_file = open(file_name,'r');
	data_list = []
	for line in input_file:
		mydict = json.loads(line)
		mylist = []
		mylist.append(mydict['label'])
		mylist.append(mydict['list'])
		data_list.append(mylist)
	input_file.close()
	return data_list

def Pr_Positive_Overall(myList,learn_index):
	overall_amount = len(myList)
	pos_amount = 0
	for i in range(0,learn_index):
		pos_amount = pos_amount + int(myList[i][0])
	return float(pos_amount)/float(learn_index)

def Pr_Word_in_Positive(word,mylist,learn_index):
	overall_positive = 0
	overall_positive_contain_word = 0
	for i in range(0,learn_index):
		if(int(mylist[i][0])==1):
			overall_positive = overall_positive + 1
			templist = mylist[i][1]
			if(word in templist):
				overall_positive_contain_word = overall_positive_contain_word + 1
	return float(overall_positive_contain_word)/float(overall_positive)
	

def Pr_Word_in_Negative(word,mylist,learn_index):
	overall_negative = 0
	overall_negative_contain_word = 0
	for i in range(0,learn_index):
		if(int(mylist[i][0])==0):
			overall_negative = overall_negative + 1
			templist = mylist[i][1]
			if(word in templist):
				overall_negative_contain_word = overall_negative_contain_word + 1
	return float(overall_negative_contain_word)/float(overall_negative)
	

def Pr_Positive_Given_Word(word,mylist,learn_index):
	PrWordInPos = Pr_Word_in_Positive(word,mylist,learn_index)
	PrWordInNeg = Pr_Word_in_Negative(word,mylist,learn_index)
	#print str(PrWordInPos)+" "+str(PrWordInNeg)
	PrPos = Pr_Positive_Overall(mylist,learn_index)
	PrNeg = 1.0 - PrPos
	PrPosGivenWord = PrWordInPos*PrPos/(PrWordInPos*PrPos+PrWordInNeg*PrNeg)
	return PrPosGivenWord

def Get_Word_Bank(mylist,learn_index):
	word_list = []
	for i in range(0,learn_index):
		word_list.extend(mylist[i][1])
	return word_list

def Pr_Positive_Given_Sentence(sent,mylist,learn_index):	
	comment_list = stem_and_remove_stopword(sent)
	word_bank = Get_Word_Bank(mylist,learn_index)
	for word in comment_list:
		if word not in word_bank:
			comment_list.remove(word)

	pr_list = []
	for word in comment_list:
		pr_list.append(Pr_Positive_Given_Word(word,mylist,learn_index))
	
	p1 = 1
	p2 = 1
	for pr in pr_list:
		p1 = p1*pr
		p2 = p2*(1-pr)
	
	return p1/(p1+p2)

def Pr_Positive_Given_Comment_List_and_Word_Bank(comment_list,data_list,word_bank,learn_index):	
	for word in comment_list:
		if word not in word_bank:
			comment_list.remove(word)

	pr_list = []
	for word in comment_list:
		pr_list.append(Pr_Positive_Given_Word(word,data_list,learn_index))
	
	p1 = 1
	p2 = 1
	for pr in pr_list:
		p1 = p1*pr
		p2 = p2*(1-pr)
	
	return p1/(p1+p2)



data_list = readFile(json_file)
learn_index = int(len(data_list)*learn_ratio)
word_list = Get_Word_Bank(data_list,learn_index)

word_set_refined = []
c = Counter(word_list)

counter = 0
for key,value in c.items():
	counter = counter + 1
	if(counter%100==0):
		print counter
	if value > 1:
		word_set_refined.append(key)
pr_dict = dict()

output = open("word_prob.json","w")
json_dump = json.dumps(pr_dict)
output.write(json_dump)
output.close()
print "Done"

