import csv
import nltk
from nltk.corpus import stopwords
import re
from nltk import stem
import json

senti_file = "SentiWordNet.txt"
stemmer = stem.snowball.EnglishStemmer()
	
def read_dataset(file_name):	
	data = []
	infile = open(file_name,'r')
	count = 0
	for line in infile:
		item = line.split()
		if float(item[2])==float(item[3])==0:
			for i in range(4,len(item)):
				if '#' in item[i] and '_' not in item[i]:
					count = count + 1
					word = item[i].split('#')
					data.append(word[0])
	print str(count)
	return data

def stem(raw_list):
	ret_list = []

	for word in raw_list:
		word = stemmer.stem(word)
		ret_list.append(word)
			
	return ret_list


data_list = read_dataset(senti_file)
uni_list = list(set(data_list))
stem_list = stem(uni_list)
output_file = open('neutral.csv','wb')
wr = csv.writer(output_file,delimiter=' ',quotechar='"',quoting=csv.QUOTE_MINIMAL)
wr.writerow(stem_list)
output_file.close()



