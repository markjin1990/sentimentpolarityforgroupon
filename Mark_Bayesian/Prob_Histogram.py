import json
import matplotlib.pyplot as plt


json_file = 'groupon_data.json'
prob_file = 'word_prob2.json'
learn_ratio = 0.7

def readProb(fileName):
	input_file = open(fileName,'r');
	for line in input_file:
		mydict = json.loads(line)
		
		return mydict


prob_dict = readProb(prob_file)
x = []
y = []
for i in range(0,20):
	y.append(0) 
	x.append(i*0.05)

for value in prob_dict.itervalues():
	for i in range(0,20):
		if value == 0.0:
			continue
		if value>=0.05*i and value<0.05*(i+1):
			y[i] = y[i] + 1

#plt.axis([0.0,1.0,0,3000])
plt.bar(x,y,width=0.05)
plt.xlabel("Probability of Positive given Word")
plt.ylabel("# of word")
plt.savefig('1.png') 
