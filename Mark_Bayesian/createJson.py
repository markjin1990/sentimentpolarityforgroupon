import csv
import nltk
from nltk.corpus import stopwords
import re
from nltk import stem
import json

groupon_file = "resturant-feedback-1.csv"
	
def read_dataset(file_name):	
	data = []
	counter=0
	with open(file_name) as f:
		reader = csv.reader(f,delimiter='\n');
		for row in reader:
			if counter==0 or counter ==1:
				counter = counter + 1
				continue
			line = []
			content = row[0]
			if(content[1]=='N'):
				continue
			line.append(content[1])
			line.append(content[2:].rstrip().lstrip())
			data.append(line)
			counter = counter + 1
	return data

def stem_and_remove_stopword(sent):
	ret_list = []
	sent = sent.lower()
	tokenized_list = nltk.word_tokenize(sent)
	stemmer = stem.snowball.EnglishStemmer()

	for word in tokenized_list:
		if re.match("[a-z]*$",word): 
			if(word[-1]=='.' or word[-1]==','):
				word = word[:-1]
			word = stemmer.stem(word)
			if word not in stopwords.words('english'):
				ret_list.append(word)
			
	return ret_list

output_file = open('groupon_data22.json','w');

data_list = read_dataset(groupon_file)
for item in data_list:
	word_list = stem_and_remove_stopword(item[1])
	data = {"label":item[0],"list":word_list}
	json_dump = json.dumps(data)
	output_file.write(json_dump+'\n')
output_file.close()



