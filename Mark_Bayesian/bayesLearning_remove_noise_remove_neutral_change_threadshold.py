import json
import csv
import nltk
import re
from nltk.corpus import stopwords
from nltk import stem
from random import choice
from collections import Counter

json_file = 'groupon_data.json'
prob_file = 'word_prob2.json'
netural_file = 'neutral.csv'
threadshold = 0
learn_ratio = 0.7
test_num = 1000

def readNetural(fileName):
	input_file = open(fileName,'r');
	reader = csv.reader(input_file,delimiter=' ')
	for row in reader:
		return row

def readProb(fileName):
	input_file = open(fileName,'r');
	for line in input_file:
		mydict = json.loads(line)
		input_file.close()
		return mydict


def readFile(file_name):
	input_file = open(file_name,'r');
	data_list = []
	pos = 0
	total = 0
	for line in input_file:
		total = total + 1
		mydict = json.loads(line)
		mylist = []
		mylist.append(mydict['label'])
		if(int(mydict['label'])==1):
			pos = pos + 1
		mylist.append(mydict['list'])
		data_list.append(mylist)
	global threadshold
	threadshold = pos/float(total)
	input_file.close()
	return data_list

def stem_and_remove_stopword(sent):
	ret_list = []
	sent = sent.lower()
	tokenized_list = nltk.word_tokenize(sent)
	stemmer = stem.snowball.EnglishStemmer()

	for word in tokenized_list:
		if re.match("[a-z]*$",word): 
			if(word[-1]=='.' or word[-1]==','):
				word = word[:-1]
			word = stemmer.stem(word)
			if word not in stopwords.words('english'):
				ret_list.append(word)
			
	return ret_list

def Pr_Positive_Overall(myList,learn_index):
	overall_amount = len(myList)
	pos_amount = 0
	for i in range(0,learn_index):
		pos_amount = pos_amount + int(myList[i][0])
	return float(pos_amount)/float(learn_index)

def Pr_Word_in_Positive(word,mylist,learn_index):
	overall_positive = 0
	overall_positive_contain_word = 0
	for i in range(0,learn_index):
		if(int(mylist[i][0])==1):
			overall_positive = overall_positive + 1
			templist = mylist[i][1]
			if(word in templist):
				overall_positive_contain_word = overall_positive_contain_word + 1
	return float(overall_positive_contain_word)/float(overall_positive)
	

def Pr_Word_in_Negative(word,mylist,learn_index):
	overall_negative = 0
	overall_negative_contain_word = 0
	for i in range(0,learn_index):
		if(int(mylist[i][0])==0):
			overall_negative = overall_negative + 1
			templist = mylist[i][1]
			if(word in templist):
				overall_negative_contain_word = overall_negative_contain_word + 1
	return float(overall_negative_contain_word)/float(overall_negative)
	

#def Pr_Positive_Given_Word(word,mylist,learn_index):
#	PrWordInPos = Pr_Word_in_Positive(word,mylist,learn_index)
#	PrWordInNeg = Pr_Word_in_Negative(word,mylist,learn_index)
#	#print str(PrWordInPos)+" "+str(PrWordInNeg)
#	PrPos = Pr_Positive_Overall(mylist,learn_index)
#	PrNeg = 1.0 - PrPos
#	PrPosGivenWord = PrWordInPos*PrPos/(PrWordInPos*PrPos+PrWordInNeg*PrNeg)
#	return PrPosGivenWord

def Contains_Prob(word):
	if word in prob_dict:
		return True
	return False	

def Pr_Positive_Given_Word(word):
	return float(prob_dict[word])

def Get_Word_Bank(mylist,learn_index):
	word_list = []
	for i in range(0,learn_index):
		word_list.extend(mylist[i][1])
	return word_list

def Pr_Positive_Given_Sentence(sent,mylist,learn_index):	
	comment_list = stem_and_remove_stopword(sent)
	word_bank = Get_Word_Bank(mylist,learn_index)
	for word in comment_list:
		if word not in word_bank:
			comment_list.remove(word)

	pr_list = []
	for word in comment_list:
		#pr_list.append(Pr_Positive_Given_Word(word,mylist,learn_index))
		if Contains_Prob(word):
			p = Pr_Positive_Given_Word(word)
			if p is 0:
				continue
			pr_list.append(p)
		else:
			continue
	
	p1 = 1
	p2 = 1
	for pr in pr_list:
		p1 = p1*pr
		p2 = p2*(1-pr)
	
	return p1/(p1+p2)

def Pr_Positive_Given_Comment_List_and_Word_Bank(comment_list,data_list,word_bank,learn_index):	
	#print comment_list
	refined_list = []	
	global netural_list
	for word in comment_list:
		if word in word_bank and word not in netural_list:
			refined_list.append(word)

	#print refined_list
	pr_list = []
	for word in refined_list:
		#pr_list.append(Pr_Positive_Given_Word(word,data_list,learn_index))
		if Contains_Prob(word):
			p = Pr_Positive_Given_Word(word)
			if p == 0:
				continue
			if p == 1:
				continue
			#if p < 0.45 and p>0.25:
			#	continue
			pr_list.append(p)
		else:
			continue
	#print pr_list	
	p1 = 1
	p2 = 1
	#print pr_list
	for pr in pr_list:
		if(pr == 0 or pr == 1):
			print "WARNING!!!"
		p1 = p1*pr
		p2 = p2*(1-pr)
		
	return p1/(p1+p2)

def Accuracy(mylist,learn_index):
	word_bank = Get_Word_Bank(mylist,learn_index)
	word_set_refined = []
	c = Counter(word_bank)

	for key,value in c.items():
		if value > 1:
			word_set_refined.append(key)
	tp = 0
	fp = 0
	fn = 0
	tn = 0
	
	pred_dict = dict()	
	real_dict = dict()	

	print "Index | Label | Predict | Probability"
	for i in range(learn_index,learn_index+test_num):
		label = mylist[i][0]
		real_dict[i] = int(label)
		comment_list = mylist[i][1]
		prob = Pr_Positive_Given_Comment_List_and_Word_Bank(comment_list,mylist,word_set_refined,learn_index)
		pred_dict[i] = prob
	
	# Estimate cut off according to threadshold percentage
	all_prob = pred_dict.values()
	all_prob.sort(reverse=True)
	list_len = len(all_prob)
	global threadshold
	cut_index = int(float(list_len)*threadshold)
	cut = all_prob[cut_index]

	for i in range(learn_index,learn_index+test_num):
		prob = pred_dict[i]
		label = real_dict[i]
		predict = 0
		if(prob>=cut):
			predict = 1
		if(int(label)==predict and predict == 1):
			tp = tp + 1
		if(int(label)==predict and predict == 0):
			tn = tn + 1
		if(int(label)!=predict and predict == 1):
			fp = fp + 1
		if(int(label)!=predict and predict == 0):
			fn = fn + 1

		print str(i)+" | "+str(label)+" | "+str(predict)+" | "+str(prob)
	precision = 1
	if((tp+fp)!=0):
		precision = float(tp)/float(tp+fp)
	recall = 1
	if((tp+fn)!=0):
		recall = float(tp)/float(tp+fn)
	accuracy = float(tp+tn)/float(tp+tn+fp+fn)
	f1 = 2*precision*recall/(precision+recall)
	print "True Positive: "+str(tp)
	print "True Negative: "+str(tn)
	print "False Positive: "+str(fp) 
	print "False Negative: "+str(fn) 
	print "Precision: "+str(precision)
	print "Recall: "+str(recall)
	print "Accuracy: "+str(accuracy)
	print "F1: "+str(f1)


data_list = readFile(json_file)
prob_dict = readProb(prob_file)
learn_index = int(len(data_list)*learn_ratio)
global netural_list
netural_list = readNetural(netural_file)
Accuracy(data_list,learn_index)

		

#for i in range(0,10):
#	word = choice(word_list[:learn_index])
#	print word+" "+str(Pr_Positive_Given_Word(word,data_list,learn_index))

#sent = "This restaurant is awesome! I had my favorite fish and strawberry pie."

#print Pr_Positive_Given_Sentence(sent,data_list,learn_index) 
